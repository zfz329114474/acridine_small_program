//app.js
const server = require("/utils/server.js");
const tool = require("/utils/util.js");
const bgImg = require("/utils/bg-imgs.js");
const comFunc = require('/utils/common.js');
App({
  onLaunch: function() {
    const _this = this;
    const systemInfo = wx.getSystemInfoSync();
    _this.globalData.statusBarHeight = systemInfo.statusBarHeight + "px";
    _this.globalData.isIPhoneX = systemInfo.model.indexOf("iPhone X") >= 0 ? true : false;
    this.getLoadData().then(function(res) {
      const versionParam = {
        member_id: res.data.member_id,
        version_id: 2,
        phone_model: systemInfo.model,
      }
      if(comFunc.compareVersion(systemInfo.SDKVersion, '1.9.90') >= 0) {
        server.postJSON('/index/login/record', versionParam, function(res) {
          if(res.data.error == 0) {
            var versionInfo = res.data.data;
            _this.globalData.versionInfo = versionInfo;
            if (versionInfo.version > 2){
              let isShowCancel = true;
              if (versionInfo.is_force == 1) {
                isShowCancel = false;
              }
              const updateManager = wx.getUpdateManager()
              updateManager.onCheckForUpdate(function(res) {
                if (res.hasUpdate) {
                  updateManager.onUpdateReady(function() {
                    wx.showModal({
                      title: '更新提示',
                      content: versionInfo.upgrade_point,
                      showCancel: isShowCancel,
                      success: function(res) {
                        if(res.confirm) {
                          updateManager.applyUpdate()
                        }
                      }
                    })
                  })
                }
              })
            }
          }
        })
      }
    })
  },
  getLoadData() {
    var _this = this;
    return new Promise(function (resolve, reject) {
      var userInfo = _this.globalData.userInfo
      if (userInfo) {
        var res = {
          error: 0,
          data: userInfo,
          message: '成功'
        }
        resolve(res)
        return;
      }
      wx.login({
        success: res => {
          if(res.code) {
            server.getJSON("/index/index/openid", {
              code: res.code
            }, function (res) {
              if (res.data.error !== 0) {
                wx.showToast({
                  title: res.message,
                  icon: 'none'
                });
                return;
              }
              _this.globalData.userInfo = res.data.data
              wx.setStorage({
                key: 'openId',
                data: res.data.data.openid,
              });
              wx.setStorageSync('userInfo', res.data.data);
              var successData = {
                error: 0,
                data: res.data.data,
                message: '成功'
              }
              resolve(successData)
            });
          } else {
            var errData = {
              message: '登陆失败',
              error: 1,
              data: null
            }
            reject(errData)
          }
        }
      });
    })
  },
  //校验是否已经填写手机号完善信息，如果没有需要弹出注册页面注册 member_mobile_bind : 0未绑定手机 1 已经绑定手机
  isMember(userInfo) {
    if (userInfo && userInfo.member_mobile) {
      return true;
    }
  },
  globalData: {
    userInfo: null,
    statusBarHeight: "20px",
    isIPhoneX: false,
    chat_BG: bgImg.chat_BG,
    member_BG: bgImg.member_BG
  }
})