// pages/components/footer/footer.js
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {

    "currentPage": {
      "type": String,
      "value": "index"
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    "isIPhoneX": app.globalData.isIPhoneX,
    "color": "#fff",
    "selectedColor": "#fff",
    "backgroundColor": "#fff",
    "borderStyle": "#fff",
    "list": [
      {
        "pageName": "index",
        "pagePath": "/pages/container/index/index",
        "text": "首页",
        "iconPath": "/images/tab/tab1.png",
        "selectedIconPath": "/images/tab/tab1_select.png"
      },
      {
        "pageName": "customization",
        "pagePath": "/pages/container/customization/customization",
        "text": "私人定制",
        "iconPath": "/images/tab/tab2.png",
        "selectedIconPath": "/images/tab/tab2_select.png"
      },
      {
        "pageName": "orderList",
        "pagePath": "/pages/container/order-list/order-list",
        "text": "订单列表",
        "iconPath": "/images/tab/tab3.png",
        "selectedIconPath": "/images/tab/tab3_select.png"
      },
      {
        "pageName": "myCenter",
        "pagePath": "/pages/container/my-center/my-center",
        "text": "个人中心",
        "iconPath": "/images/tab/tab4.png",
        "selectedIconPath": "/images/tab/tab4_select.png"
      }
    ]
  },
  attached: function () {
   
  },
  /**
   * 组件的方法列表
   */
  methods: {
    redirectTo: function(e) {
      var navigatorurl = e.currentTarget.dataset.navigatorurl
      if ("" == navigatorurl) {
        return;
      }
      wx.redirectTo({
        "url": navigatorurl
      });
    }
  }
})
