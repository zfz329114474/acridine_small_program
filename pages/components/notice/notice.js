// pages/components/notice/notice.js
const app = getApp();
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js");
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    member_id: String,
  },

  /**
   * 组件的初始数据
   */
  data: {
    animationData: {},
    msgData: {},
    showMsg: {},
    opacity: 0,
    isShow: false,
    noticeTime: null,
    userInfo:{},
    height: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 查看地址
    showLogistics: function(evt) {
      const position = tool.getDataset(evt);
      
      wx.openLocation({
        latitude: parseFloat(position.latitude),
        longitude: parseFloat(position.longitude),
        name: "厦门艾美酒店",
        success: function(res) {
          console.log(res)
        }
      });
    },
    // 清除定时器停止
    clearNoticeTime: function() {
      clearTimeout(this.data.noticeTime)
    },
    // 加载未读列表
    loadNotice: function(param) {
      const _this = this;
      wx.showNavigationBarLoading();
      param.getMsgType = "index";
      server.postJSON("/index/chat/selectmsg", param, function(res) {
        wx.hideNavigationBarLoading()
        if (res.data.error != 0) {
          tool.warning(res.data.message);
          return;
        }
        for (var i = 0; i < res.data.data.data.length; i++) {
          let date = new Date(res.data.data.data[i].add_time * 1000)
          res.data.data.data[i].monthDay = tool.formatDate(date, "mm-dd");
        }
        _this.setData({
          msgData: res.data.data
        });
        _this.getNextNotice();
        _this.data.noticeTime = setTimeout(function(){
          _this.loadNotice(param);
        },1000 * 20);
      });
    },
    getNextNotice: function() {
      const _this = this;
      const query = wx.createSelectorQuery().in(_this)
      //获取未读消息
      const unreadMsgList = _this.data.msgData.data.filter(function(item, index) {
        if (item.is_read == 0) {
          return item;
        }
      });
      var animation;
      if (unreadMsgList.length > 0) {
        const unreadMsg = unreadMsgList[0];
        _this.setData({
          isShow: true,
          showMsg: unreadMsg
        });
        var height;
        query.select("#notice-wrap").boundingClientRect(function(res) {  
          height = res.height;
          var count = _this.data.count + 1
          _this.setData({
            count: count
          })
          if(height == 0 ) {
            var timeout = setTimeout(()=>{
              _this.getNextNotice()
              clearTimeout(timeout)
            },500)
          } else {
            animation = wx.createAnimation();
            animation.opacity(1).height(height).step();
            _this.setData({
              animationData: animation.export()
            });
          }
        }).exec();
      } else {
        animation = wx.createAnimation();
        _this.setData({
          isShow: false
        });
      }
    },
    // 关闭消息
    closeNotice: function() {
      const _this = this;
      var animation = wx.createAnimation();
      animation.opacity(0).height(0).step();
      _this.setData({
        animationData: animation.export()
      })
      let msgData = _this.data.msgData;
      for (var i = 0; i < msgData.data.length; i++) {
        if (msgData.data[i].sid == _this.data.showMsg.sid) {
          msgData.data[i].is_read = 1;
        }
      }

      _this.setData({
        msgData: msgData
      });

      setTimeout(function() {
        _this.getNextNotice()
      }, 1000);
      server.postJSON("/index/chat/read", {
        record: _this.data.showMsg.sid,
        member_id: _this.properties.member_id
      }, function(res) {

      });
    },
    stopChat() {

    },
    jumpClick: function(evt) {
      let jump_url;
      const _this = this;
      const clickedMsg = _this.data.msgData.data.filter(function(item){
        if (item.sid == evt.currentTarget.dataset.msgId) {
          return item;
        }
      })[0];
      console.log(clickedMsg)
      const normalGoodsLabel = [1, 2, 3, "1", '2', '3'];
      const activityGoodsLabel = [4, 5, 6, 7, '4', '5', '6', '7'];
      if (clickedMsg.getMsgType == 'aindex' ||clickedMsg.getMsgType == 'oindex' || clickedMsg.getMsgType == 'goods') {
        if (normalGoodsLabel.indexOf(clickedMsg.order_label) || clickedMsg.order_label == 0) {
          jump_url = '/pages/container/goods-details/goods-details?goodsId=' + clickedMsg.goods_id;
        } else if (activityGoodsLabel.indexOf(lickedMsg.order_label)) {
          jump_url = '/pages/container/goods-details/activity/activity?uaId=' + clickedMsg.goods_id
        }
      } else  {
        console.log(normalGoodsLabel.indexOf(clickedMsg.order_label))
        if (clickedMsg.order_label == 0) {
          jump_url = '/pages/container/order-details/order-details?order_id=' + clickedMsg.order_id + "&order_label=" + clickedMsg.order_label
        } else if ( normalGoodsLabel.indexOf(clickedMsg.order_label) >= 0) {
          jump_url = '/pages/container/order-details/order-details?all_no=' + clickedMsg.all_no + "&order_label=" + clickedMsg.order_label
        } else {
          jump_url = '/pages/container/order-details/activity-payed/activity-payed?all_no=' + clickedMsg.all_no + "&order_label=" + clickedMsg.order_label
        }
      }
      if(jump_url) {
        _this.triggerEvent("navigator", { jump_url: jump_url });
      }
    }
  }
})