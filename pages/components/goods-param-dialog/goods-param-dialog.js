// pages/components/goods-param-dialog/goods-param-dialog.js
const tool = require("../../../utils/util.js"); 
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    goodsDetails: {
      type: Object
    },
    optionModelName:{
      type: String
    },
    buyGoodsNum: {
      type: Number
    },
    memberAvatarList: Array,
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShow: false,
    modalName: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // _stopTapHandle: function(){

    // },
    /**
     * 阻止页面滚动
     */
    // _move: function(){

    // },
    /**
     * 显示月定制月份选择
     */
    _showSelectMonth: function(){
      this.triggerEvent("showChooseMonth")
    },
    /**
     * 选择月份 增加数量
     */
    _addMonthNum(e) {
      const _this = this;
      const month = tool.getDataset(e).month;
      _this.triggerEvent("addMonthNum", { month: month })
    },
    // 月份选择与取消
    _toggleMonthNum(e){
      const _this = this;
      const setData = tool.getDataset(e);
      _this.triggerEvent("toggleMonthNum",setData)
    },
    /**
     * 选择月份 减少数量
     */
    _minusMonthNum(e) {
      const _this = this;
      const month = tool.getDataset(e).month;
      _this.triggerEvent("minusMonthNum", { month: month });
    },
    // 月定制 选择月份后购买
    _monthCustomBuy: function(){
      const _this = this;
      _this.triggerEvent("monthCustomBuy");
    },
    _addGoodsNum(){
      this.triggerEvent("addGoodsNum");
    },
    _minusGoodsNum() {
      this.triggerEvent("minusGoodsNum");
    },
    //搭配购选择规格
    _changeMatchSpec: function(e) {
      console.log(e);
      const _this = this;
      const dataset = tool.getDataset(e);
      const matchSpecIndex = dataset.specIndex;
      const matchId = dataset.matchId;
      let matchGoods = _this.properties.goodsDetails.collocation.filter(function(item){
        if(item.goods_id == matchId) {
          return item;
        }
      })[0];
      // 如果重复点击 判断
      if(matchSpecIndex == matchGoods.defaultSpecIndex) {
        return;
      }
      // 判断规格 库存是否充足
      if (matchGoods.spec[matchSpecIndex].goods_storage <= 0) {
        tool.warning("该规格，库存不足");
        return;
      }
      _this.triggerEvent("changeMatchSpec", { matchSpecIndex: matchSpecIndex, matchId: matchId });
    },
    // 搭配购减少数量
    _matchMinus: function(e) {
      const _this = this;
      const matchIndex = tool.getDataset(e).matchIndex;
      this.triggerEvent("matchMinus", { matchIndex: matchIndex });
    }, 
    // 搭配购增加数量
    _matchAdd: function(e) {
      const _this = this;
      const matchIndex = tool.getDataset(e).matchIndex;
      this.triggerEvent("matchAdd", { matchIndex: matchIndex });
    },
    // 搭配购确定
    _macthConfirm: function(e) {
      const _this = this;
      _this.triggerEvent("macthConfirm");
    },
    // 跟换规格
    _changeSpec(e) {
      const _this = this;
      const specIndex = tool.getDataset(e).specIndex;
      //判断是否已经选中该规格 
      if (_this.properties.goodsDetails.defaultSpecIndex == specIndex) {
        return;
      }
      // 判断该规格库存是否无库存如果无库存 不然选
      if (_this.properties.goodsDetails.spec[specIndex].goods_storage <= 0) {
        return;
      }
      _this.triggerEvent("changeSpec", { defaultSpecIndex: specIndex });
    },
    // 加入购物车
    _addCartHandle: function(){
      this._hideDialog();
      this.triggerEvent("addCartHandle");
    },
    // 立即定制
    _goBuyGoods: function () {
      this._hideDialog();
      this.triggerEvent("buyGoodsHandle");
    },

    //隐藏 弹窗
    _hideDialog(){
      const _this = this;
      _this.setData({
        isShow: false
      });
    },
    // 显示弹窗
    _showDialog() {
      const _this = this;
      this.setData({
        isShow: true
      });
    },
    // 显示选择月定制
    _show_modal_buy:function(){
      const _this = this;
      _this.triggerEvent("chooseMatchBuy")
    },
    showDialog() {
      this._showDialog();
    },
    cancelEvent(){
      this._hideDialog();
    }

  }
})
