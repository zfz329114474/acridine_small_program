/* pages/components/goods-param-dialog/goods-param-dialog.wxss */
.modal {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 1900;
}
.modal-dialog {
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
}
.modal-content {
  width: calc(100% - 60rpx);
  margin: 0 30rpx;
  background-color: #fff;
  border-radius: 8rpx;
}
.flex-grow {
  flex-grow: 2;
}
.modal-header {
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20rpx 40rpx;
}
.modal-body {
  padding: 0 20rpx;
  max-height: 800rpx;
  box-sizing: border-box;
  /* overflow-y: scroll; */
}
.modal-footer {
  padding: 40rpx 20rpx;
}
.modal-header .title {
  line-height: 56rpx;
  font-size: 40rpx;
  color: #4A4A4A;
}
.modal-header .modal-cancel-btn {
  color: #ef364e;
  font-size: 32rpx;
}
.modal-body .goods-intro-container {
  display: flex;
  justify-content: space-between;
  padding-top: 40rpx;
  margin-bottom: 60rpx;
  border-top: 1rpx solid #dcdcdc;
}
.modal-body .goods-intro-container .goods-synopsis {
  display: flex;
  flex-direction: column;
  width: 430rpx;
}
.modal-body .goods-intro-container .good-title {
  overflow: hidden;
  height: 50rpx;
  width: 430rpx;
  margin-bottom: 5rpx;
  line-height: 50rpx;
  text-overflow: ellipsis;
  white-space: nowrap;
  color: #4A4A4A;
  font-size: 36rpx;
}
.modal-body .goods-intro-container .sub-goods-title {
  margin-bottom: 30rpx;
  font-size: 28rpx;
  color: #9b9b9b;
  line-height: 40rpx;
}
.modal-body .goods-thumb-container {
  width: 170rpx;
  height: 144rpx;
  flex-shrink: 0;
}
.modal-body .goods-thumb-container .goods-thumb {
  width: 100%;
  height: 100%;
}
.modal-body .goods-specification {
  display: flex;
  flex-wrap: wrap;
  padding-bottom: 40rpx;
}
.modal-body .goods-specification text {
  padding: 20rpx 30rpx;
  margin-right: 20rpx;
  margin-bottom: 20rpx;
  border: 1rpx solid #e6e6e6;
  border-radius: 8rpx;
  color: #67666b;
  font-size: 28rpx;
}
.modal-body .goods-specification .goods-active {
  background-color: #F8F8F8;
}
.modal-body .select-quantity {
  display: flex;
  align-items: center;
  padding-bottom: 60rpx;
  border-bottom: 1rpx solid #DCDCDC;
}
.modal-body .select-quantity .select-num-btn {
  width: 100rpx;
  height: 60rpx;
}
.modal-body .select-quantity .selected-num {
  min-width: 30rpx;
  padding: 0 30rpx;
  text-align: center;
  font-size: 36rpx;
  color: #000;
}
.modal-body .macth-buy-container {
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 40rpx 10rpx 40rpx 20rpx;
  border-bottom: 1rpx solid #DCDCDC;
}
.modal-body .macth-buy-container .icon-diamond {
  width: 50rpx;
  height: 50rpx;
  margin-right: 30rpx;
}
.modal-body .macth-buy-container .icon-triangle {
  width: 18rpx;
  height: 24rpx;
}
.modal-body .macth-buy-container .macth-explain {
  color: #4A4A4A;
  font-size: 32rpx; 
}
.modal-footer {
  display: flex;
  justify-content: space-between;
}
.modal-footer .add-cart {
  width: 280rpx;
  height: 90rpx;
  line-height: 90rpx;
  border-radius: 8rpx;
  background-color: #f8f8f8;
  text-align: center;
  font-size: 28rpx;
  color: #67666B;
}
.modal-footer .go-buy {
  width: 280rpx;
  height: 90rpx;
  line-height: 90rpx;
  border-radius: 8rpx;
  background: linear-gradient(-180deg, #F64565 0%, #EF364E 100%);
  box-shadow: 0 10px 10px 0 rgba(244,63,92,0.15);
  color: #fff;
  font-size: 30rpx;
  text-align: center;
}
.big-confirm-btn {
  width: 100%;
  height: 90rpx;
  background-image: linear-gradient(-180deg, #F64565 0%, #EF364E 100%);
  box-shadow: 0 20rpx 20rpx 0 rgba(244,63,92,0.15);
  border-radius: 8rpx;
  color: #fff;
  line-height: 90rpx;
  text-align: center;
  font-size: 30rpx;  
}
.modal-body .group-people-container {
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 25rpx 0;
}
.modal-body .group-people-container .group-people-title {
  position: relative;
  padding-left: 36rpx;
  font-size: 24rpx;
  color: #9B9B9B; 
  line-height: 34rpx;
  margin-bottom: 60rpx;
}
.modal-body .group-people-container .group-people-title::after {
  position: absolute;
  top: 10rpx;
  left: 0;
  width: 12rpx;
  height: 12rpx;
  border: 1rpx solid #9B9B9B;
  border-radius: 50%;
  content: "";
}
.modal-body .group-people-container .group-people-list {
  display: flex;
  justify-content: center;
}
.modal-body .group-people-container .group-people-avatar {
  width: 80rpx;
  height: 80rpx;
  border-radius: 50%;
  margin: 0 35rpx;
}
.modal .month-custom-bottom-line {
  position: relative;
}
.modal .month-custom-bottom-line::after {
  position: absolute;
  left: 20rpx;
  right: 20rpx;
  bottom: 0;
  height: 2rpx;
  background-color: #DCDCDC;
  content: "";
}
.modal-body .month-custom-explain {
  position: relative;
  padding-left: 36rpx;
  color: #9B9B9B;
  font-size: 24rpx;
}
.modal-body .month-custom-explain::after {
  position: absolute;
  top: 10rpx;
  left: 5rpx;
  width: 12rpx;
  height: 12rpx;
  border: 1rpx solid #9b9b9b;
  border-radius: 50%;
  content: "";
}
.modal-body .month-scroll {
  overflow: hidden;
  max-height: 550rpx;
}
.modal-body .month-scroll .month-scroll-item {
  display: flex;
  align-items: center;
  padding: 25rpx 0;
  height: 50rpx;
}
.modal-body .month-scroll .icon-circle {
  width: 40rpx;
  height: 40rpx;
  overflow: hidden;
  border-radius: 50%;
  margin-right: 30rpx;
  box-shadow: 2rpx 0 #c4c4c4 inset,
              0 2rpx #c4c4c4 inset,
              -2rpx 0 #c4c4c4 inset,
              0 -2rpx #c4c4c4 inset;
}
.modal-body .month-scroll .icon-circle .select {
  width: 100%;
  height: 100%;
}
.modal-body .month-scroll-item .select-num-btn {
  width: 100rpx;
  height: 65rpx;
}
.modal-body .month-scroll-item .month-selected-num {
  padding: 0 20rpx;
}