// pages/components/login/login.js
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js");
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {},

  /**
   * 组件的初始数据
   */
  data: {
    loginData: {
      openid: wx.getStorageSync('userInfo').openid,
    },
    password_type: 'password',
    codeBtnText: "发送验证码",
    countDownTime: 60,
    isSendCode: false, 
    isShow: false,
    timer: null,
    loginWay: "phoneLogin",
    // loginWay: "setPassword"
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 转换是否显示密码
    targetPassWDType: function(){
      const _this = this;
      var password_type = _this.data.password_type;
      password_type = password_type == 'text' ? 'password' : 'text';
      _this.setData({
        password_type: password_type
      })
    },
    goAccountLogin: function(e) {
      this.setData({
        loginWay: "accountLogin"
      });
    },
    hiddenModel: function() {
      this.setData({
        isShow: false
      });
    },
    _changeInputValue: function(e) {
      tool.changInput( e, this);
    },
    showModel: function() {
      const _this = this;
      app.getLoadData().then(function(res) {
        console.log('res')
        console.log(res)
        console.log('res')
        if(res.error == 0) {
          _this.setData({
            "loginData.member_id": res.data.member_id,
            "loginData.openid": res.data.openid,
            isShow: true
          });

        }
      })
    },
    // 发送验证码 
    sendCode: function() {
      const _this = this;
      const loginData = _this.data.loginData;
      if (!loginData.member_mobile || loginData.member_mobile == "") {
        tool.warning("请输入手机号");
        return;
      } else if (!tool.isValidMobile(loginData.member_mobile)) {
        tool.warning("手机号格式不正确");
        return;
      }
      let lastTs = wx.getStorageSync('sms-ts');
      if (lastTs == undefined) {
        lastTs = 0;
      }
      const curTs = (new Date()).getTime();
      const delta = curTs - lastTs;
      if (delta < 60 * 1000) {
        tool.warning("短信发送太频繁了，请您稍后再试。");
        return;
      }
      wx.setStorageSync('sms-ts', curTs);
      // 启动定时器
      _this.data.timer = setInterval(function() {
        _this._countDown()
      }, 1000);
      // loginData.PhoneNumbers = loginData.member_mobile;
      const param = {
        member_mobile : loginData.member_mobile,
        code: loginData.code,
      }
      wx.showNavigationBarLoading()
      server.postJSON("/index/action/getCode", param, function(res) {
        wx.hideNavigationBarLoading()
        if (res.data.error != 0) {
          tool.warning(res.data.message);
          _this.resetTime();
          return;
        }
        _this.setData({
          'loginData.token': res.data.message
        });
        tool.success("验证码已发送。");
      });
    },
    resetTime: function() {
      const _this = this;
      clearInterval(_this.data.timer);
      wx.setStorageSync('sms-ts', 0);
      _this.setData({
        countDownTime: 60,
        "codeBtnText": "发送验证码"
      });
    },
    // 倒计时
    _countDown: function() {
      const _this = this;
      let countDownTime = _this.data.countDownTime;
      if (countDownTime <= 0) {
        _this.resetTime();
        return;
      }
      
      countDownTime--;
      _this.setData({
        countDownTime: countDownTime,
        "codeBtnText": countDownTime + "秒"
      });

    },
    // 验证码登陆
    _loginCode: function(e) {
      const _this = this;
      const loginData = _this.data.loginData;

      if (!loginData.member_mobile || loginData.member_mobile == "") {
        tool.warning("请输入手机号");
        return;
      } else if (!tool.isValidMobile(loginData.member_mobile)) {
        tool.warning("手机号格式不正确");
        return;
      } else if (loginData.code == ""){
        tool.warning("请输入验证码");
        return;
      }
      wx.showNavigationBarLoading()
      server.postJSON("/index/action/verifiCation",loginData, function(res){
        wx.hideNavigationBarLoading()
        if(res.data.error != 0) {
          tool.warning(res.data.message);
          return;
        }
        _this._hiddenModel();
        _this.triggerEvent('loadSuccess',{userInfo: res.data.data});
      });
    },
    //密码登陆
    _loginPassword: function() {
      const _this = this;
      const loginData = _this.data.loginData;
      console.log(loginData);
      if (!loginData.member_mobile) {
        tool.warning("请输入手机号");
        return;
      } else if (!tool.isValidMobile(loginData.member_mobile)) {
        tool.warning("手机号格式不正确");
        return;
      } else if (loginData.member_passwd == "") {
        tool.warning("密码不能为空");
        return;
      }
      wx.showNavigationBarLoading();
      server.postJSON("/index/login/login", loginData, function(res) {
        wx.hideNavigationBarLoading();
        if (res.data.error != 0) {
          tool.warning(res.data.message);
          return;
        }
        _this._hiddenModel();
        _this.triggerEvent('loadSuccess', { userInfo: res.data.data });
      });
    },
    _hiddenModel: function() {
      this.resetTime();
      this.hiddenModel()
    },
    goPhoneLogin: function(e) {
      this.setData({
        loginWay: "phoneLogin"
      })
    }
  }
})