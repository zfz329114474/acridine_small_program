// pages/components/chat/chat.js
const app = getApp();
const tool = require("../../../utils/util.js");
const server = require("../../../utils/server.js");
const config = require("../../../config.js");
const chatBG = require("../../../utils/bg-imgs.js")
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    height: String,
    chat_BG: {
      type: String,
      value: chatBG.chat_BG
    },
    order_id: Number,
    order_label: Number, // goods 与order 统一使用
    goods_id: Number,
    fromPage: String,
    msgData: Object,
    serverData: Object
  },

  /**
   * 组件的初始数据
   */
  data: {
    "isIPhoneX": app.globalData.isIPhoneX,
    "userInfo": app.globalData.userInfo,
    "isHidden": true,
    "animationData": {},
    "scrollTop": 1000,
  },
  attached: function() {

  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 拨打电话
    callPhone: function(){
      const _this = this;
      wx.makePhoneCall({
        phoneNumber: _this.properties.serverData.service_tel,
      })
    },
    _hiddenModal: function() {
      const pages = getCurrentPages();
      if (pages[0].route === 'pages/container/customization/customization') {
        wx.navigateBack({
          delta: 1	
        });
      } else {
        this.setData({
          isHidden: true
        });
      }
    },
    wxParseImgTap: function(e){
      var _this = this;
      var nowImgUrl = e.target.dataset.src;
      var tagFrom = e.target.dataset.from;
      // if (typeof (tagFrom) != 'undefined' && tagFrom.length > 0) {
        wx.previewImage({
          // current: nowImgUrl, // 当前显示图片的http链接
          urls: [nowImgUrl] // 需要预览的图片http链接列表
        })
      // }
    },
    _showModal: function() {
      const _this = this;
      _this.setData({
        isHidden: false,
        userInfo: wx.getStorageSync('userInfo')
      })
      var query = wx.createSelectorQuery().in(_this)
      query.select('#scroll-container').boundingClientRect(function (res) {
        setTimeout(function(){
          _this.setData({
            scrollTop: res.height
          });
        },400);
      }).exec()
      
    },
    showModal: function() {
      this._showModal()
    },
    scroll: function(){
      var scrollTop = this.data.scrollTop
      console.log(scrollTop) 
      this.setData({
        scrollTop: scrollTop
      })
    },
    // 监听 输入框的字符变化
    _inputChange: function(evt) {
      tool.changInput(evt, this);
    },
    // 发送文本消息
    sendMsg: function() {
      const _this = this;
      if (_this.data.send.text) {
        let param = {
          member_id: _this.data.userInfo.member_id,
          ser_id: _this.properties.serverData.ser_id,
          is_mag: 22,
          text: _this.data.send.text,
          s_type: "text"
        }
        if (_this.properties.fromPage == "order") {
          param.order_id = _this.properties.order_id;
          param.getMsgType = "order";
          param.order_label = _this.properties.order_label;
        }
        if (_this.properties.fromPage == "goods") {
          param.goods_id = _this.properties.goods_id;
          param.getMsgType = "goods";
          param.order_label = _this.properties.order_label;
        }
        server.postJSON("/index/chat/addmsg", param, function(res) {
          _this.setData({
            "send.text": ""
          })
          if (res.data.error != 0) {
            tool.warning(res.data.message);
            return;
          }
          _this.triggerEvent('resetMsg', { fromPage: _this.properties.fromPage });
        })
      }
    },
    chooseImage: function() {
      const _this = this;
      let formData = {
        member_id: _this.data.userInfo.member_id,
        ser_id: _this.properties.serverData.ser_id,
        is_mag: 22,
        s_type: "img"
      }
      if (_this.properties.fromPage == "order") {
        formData.order_id = _this.properties.order_id;
        formData.getMsgType = "order"
        formData.order_label = _this.properties.order_label;
      }
      if (_this.properties.fromPage == "goods") {
        formData.goods_id = _this.properties.goods_id;
        formData.getMsgType = "goods"
      }
      console.log('img formData:  ', formData )
      wx.chooseImage({
        count: 1,
        success: function(res) {
          var tempFilePaths = res.tempFilePaths;
          wx.uploadFile({
            url: config.baseApiUrl + '/chat/upload',
            filePath: tempFilePaths[0],
            name: 'file',
            formData: formData,
            success: function(res) {
              console.log(res)
              _this.triggerEvent('resetMsg', { fromPage: _this.properties.fromPage});
            }
          })
        }
      })
    }
  }
})