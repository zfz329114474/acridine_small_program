// pages/component/header/header.js
let app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    "backBtnHidden": {
      "type": Boolean,
      "value": true
    },
    "title": {
      "type": String,
      "value": "吖咪"
    },
    "navigationBarBackgroundColor": {
      "type": String,
      "value": "#fff"
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    "statusBarHeight": app.globalData.statusBarHeight
  },
  attached: function () {
   
  },
  /**
   * 组件的方法列表
   */

  methods: {

      
  }
})
