// pages/container/confirm-order-details/activity-confirm-order/activity-confirm-order.js
const app = getApp();
const tool = require("../../../../utils/util.js");
const server = require("../../../../utils/server.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    order_message: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let confirmData = JSON.parse(options.confirmData);

    const userInfo = wx.getStorageSync('userInfo');
    const _this = this;
    // _this.getUserinfo()
    _this.setData({
      pageShow: true,
      isIPhoneX: app.globalData.isIPhoneX,
      confirmData: confirmData,
      userInfo: userInfo
    });

  },
  /**
   * 提交订单
   */
  creatOrder: function() {
    if (!tool.lockClick()) {
      return;
    }
    const _this = this;
    const confirmData = _this.data.confirmData;
    const param = {
      order_label: confirmData.order_label,
      member_id: _this.data.userInfo.member_id,
      ua_id: confirmData.ua_id,
      item_num: confirmData.item_num,
      order_message: _this.data.order_message,
      order_amount: confirmData.total_price, // 宴会价格 + 额外费用 （或减 折扣）
      items_amount: confirmData.total_price, // 宴会人数 成 单价
    }
    server.postJSON("/index/buy/create_op_order", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      if (res.data.data.order_state == 20) {
        tool.success("支付完成");
        setTimeout(function() {
          wx.navigateTo({
            url: '/pages/container/order-details/activity-payed/activity-payed?all_no=' + res.data.data.all_no + '&order_label=' + res.data.data.order_label,
          })
        }, 1000);
        return;
      }
      _this.toPay(res.data.data);
    });
  },
  /**
   * 是否使用钱包支付
   */
  switchChange: function (e) {
    const _this = this;
    var isWalletPay = e.detail.value;
    if (_this.data.userInfo.available_predeposit <= 0) {
      isWalletPay = false;
    }
    this.setData({
      isWalletPay: isWalletPay
    })
  },
  // 调起支付
  toPay: function(orderInfo) {
    const _this = this;
    let param = {
      all_no: orderInfo.all_no,
      order_label: orderInfo.order_label,
      openid: _this.data.userInfo.openid
    }
    if (_this.data.isWalletPay) {
      param.is_wallet = 2;
    } else {
      param.is_wallet = 1;
    }
    server.postJSON("/index/Wxpay/index", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }

      if (res.data.data.order_state == 20) {
        tool.success('支付成功');
        setTimeout(function(){
          wx.switchTab({
            // url: '/pages/container/order-details/order-details?all_no=' + param.all_no + "&order_label=" + param.order_label
            url: '/pages/container/order-list/order-list'
          });
        },1000);
        return;
      }
      wx.requestPayment({
        timeStamp: res.data.data.timeStamp,
        nonceStr: res.data.data.nonceStr,
        package: res.data.data.package,
        signType: 'MD5',
        paySign: res.data.data.paySign,
        success: function(r) {
          if (r.errMsg == "requestPayment:ok") {
            wx.navigateTo({
              url: '/pages/container/order-details/order-details?all_no=' + orderInfo.all_no + "&order_label=" + orderInfo.order_label
              // url: '/pages/container/order-list/order-list'
            })
          }
        },
        fail: function(res) {
          tool.warning("支付失败");
          wx.navigateTo({
            // url: '/pages/container/order-details/order-details?all_no=' + orderInfo.all_no+"&order_label="+ orderInfo.order_label,
            url: '/pages/container/order-details/activity-to-pay/activity-to-pay?all_no=' + orderInfo.all_no + "&order_label=" + orderInfo.order_label
          })

        }
      })
    });
  },
  /**
   * 设置订单留言
   */
  _messageChange: function(e) {
    const inputData = tool.inputChange(e);
    tool.changInput(e,this);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    pageShow: false
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})