// pages/container/index/index.js
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js")
const goodsData = require("../../../utils/data.js");
const common = require("../../../utils/common.js");

var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    pageShow: false,
    scale: 1,
    goodsData: [],
    goodsType: [],
    showTab: "recommend"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    wx.showLoading({
      title: '加载中',
    })
    app.getLoadData().then(function(res) {
      _this.loadPage();
      // 获取站内通知
      // 获取用户信息
      // _this.getUserInfo();
      app.globalData.userInfo = res.data;
      _this.setData({
        userInfo: res.data
      })
      _this.loadGooodsType({});
    },function(res){

    })
   
  },
  cartTouchmove: function(e) {
    common.cartTouchmove(e, this);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const _this = this;

    _this.notice = this.selectComponent("#notice");
    var timer = setInterval(function(){
      if(_this.data.userInfo) {
        _this.notice.loadNotice({
          member_id: _this.data.userInfo.member_id
        });
        clearInterval(timer)
      } 
    },100)
    
  },
  loadPage: function(isPullDown) {
    var _this = this;
    wx.showLoading({
      title: '加载中',
    });
    server.postJSON("/index/goods/goods_index", function(res) {
      _this.setData({
        pageShow: true
      });
      wx.hideLoading();
      if (isPullDown) {
        wx.stopPullDownRefresh();
      }
      if (res.data.error !== 0) {
        wx.showToast({
          title: res.message
        });
        return;
      }
      let goodsList = res.data.data;
      for (var i = 0; i < goodsList.length; i++) {
        if (goodsList[i].type === "recommend") {
          for (var j = 0; j < goodsList[i].data.length; j++) {
            if (goodsList[i].data[j].goods_label == 1) {
              goodsList[i].data[j].goods_label = "月定制";
            }
            if (goodsList[i].data[j].goods_label == 2) {
              goodsList[i].data[j].goods_label = "拼团";
            }
            if (goodsList[i].data[j].goods_label == 3) {
              goodsList[i].data[j].goods_label = "众筹";
            }
          }
        }
        if (goodsList[i].type === "goods_select") {
          for (var j = 0; j < goodsList[i].data.length; j++) {
            goodsList[i].data[j].goods_image = goodsList[i].data[j].goods_imgs.split(",")[0];
          }
        }
      }
      _this.setData({
        goodsData: goodsList
      });
    });
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var isPullDown = true;
    this.loadPage(isPullDown);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const _this = this;
    if (_this.notice) {
      _this.notice.loadNotice({
        member_id: _this.data.userInfo.member_id
      });
    }
  },
  swithTap: function(e) {
    this.setData({
      showTab: e.target.dataset.classify
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.notice.clearNoticeTime();
  },
  getUserInfo: function() {
    const _this = this;
    tool.getUserInfo(function(userInfo) {
      let param = {
        member_id: userInfo.member_id
      }
      _this.setData({
        userInfo: userInfo
      });
      
    });
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  loadGooodsType: function(param) {
    const _this = this;
    server.postJSON("/index/goods/gooods_typew", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return
      }
      _this.setData({
        goodsType: res.data.data
      })
    })
  },
  /**
   * 点击消息跳转
   */
  _navigator: function(evt) {
    wx.navigateTo({
      url: evt.detail.jump_url,
    });
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  /**
   * 监听页面滚动
   */
  onPageScroll: function(e) {
    var scrolltop = e.scrollTop;
    if (scrolltop > 300) {
      scrolltop = 300;
    }
    var scale = (600 - scrolltop) / 600;
    if (scale > 1) {
      scale = 1;
    }
    this.setData({
      scale: scale
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    console.log(res)
    if (res.from == "button") {
      const dataset = res.target.dataset;
      return {
        path: "/pages/container/goods-details/goods-details?goodsId=" + dataset.goods_id + "&type=share&cantuanid=" + dataset.cantuanid,
        imageUrl: dataset.shareImg
      }
    }
  }
})