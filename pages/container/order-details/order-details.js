// pages/container/order-details/order-details.js
const app = getApp();
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js");
const chat_BG = require("../../../utils/bg-imgs.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageShow:false,
    orderInfo: {},
    userInfo: {},
    msgData: [], 
    orderName: "",
    chat_BG: chat_BG.chat_BG
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    app.getLoadData().then(function(res){
      if(res.error != 0) {
        tool.warning(res.message);
        return;
      }
      _this.setData({
        userInfo: res.data
      })
      _this.loadPage(options);
    })
  },
  loadPage(param) {
    const _this = this;
    var url = "/index/order/order_info2";
    param.member_id = _this.data.userInfo.member_id;
    // 	0普通商品 1月定制 2拼团 3众筹 4私人定制 5世界厨房 6官方组局 7美食旅行
    if (param.order_label == 1 || param.order_label == 2 || param.order_label == 3) {
      url = "/index/order/order_info";
    }
    server.postJSON(url,  param, function(res) {
      _this.setData({
        pageShow: true
      })
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      const chartParam = {
        member_id: _this.data.userInfo.member_id,
        ser_id: res.data.data.ser_id,
        all_no: res.data.data.all_no,
        order_sn: res.data.data.order_sn,
        getMsgType: "order"
      }
      _this.getMesList(chartParam);
      _this.getServerData({
        ser_id: res.data.data.ser_id
      });
      let orderInfo = res.data.data;
      for (var j = 0; j < orderInfo.goods_list.length; j++) {
        if (orderInfo.goods_list[j].item_image) {
          orderInfo.goods_list[j].goods_image = orderInfo.goods_list[j].item_image
          orderInfo.goods_list[j].goods_num = orderInfo.goods_list[j].item_num
          orderInfo.goods_list[j].goods_name = orderInfo.goods_list[j].item_name
          orderInfo.goods_list[j].goods_price = orderInfo.goods_list[j].item_price
          orderInfo.goods_list[j].goods_spec = orderInfo.goods_list[j].goods_spec
        }
      }
      // 月定制转换数据格式
      if (orderInfo.order_label == 1) {
        const goods_num = orderInfo.goods_list.reduce(function(prev, next) {
          return prev += next.goods_num;
        }, 0);
        orderInfo.order_list = [{
          goods_image: orderInfo.goods_list[0].goods_image,
          goods_num: goods_num,
          order_id: orderInfo.goods_list[0].order_id,
          goods_name: orderInfo.goods_list[0].goods_name,
          goods_spec: orderInfo.goods_list[0].goods_spec,
          order_label: orderInfo.order_label,
          goods_price: orderInfo.goods_list[0].goods_price,
          monthlist: orderInfo.goods_list
        }];
      } else {
        orderInfo.order_list = orderInfo.goods_list;
      }
      if (orderInfo.order_label == 0 ||
        orderInfo.order_label == 1 ||
        orderInfo.order_label == 2 ||
        orderInfo.order_label == 3
      ) {
        _this.setData({
          orderName: "general_order"
        });
      }
      orderInfo.add_time = tool.formatDate(new Date(orderInfo.add_time * 1000), "yyyy-mm-dd hh:ii");
      _this.setData({
        orderInfo: orderInfo
      })
    });
  },
  // 获取客服信息
  getServerData: function (options) {
    const _this = this;
    server.postJSON("/index/chat/ser_info", options, function (res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        serverData: res.data.data
      });
    })
  },
  //重置消息显示
  _resetMsg: function() {
    const _this = this;
    // member_id: _this.data.userInfo.member_id,
    // ser_id: res.data.data.ser_id,
    //   all_no: res.data.data.all_no,
    //     order_sn: res.data.data.order_sn,
    //       getMsgType: "order"
    const chartParam = {
      member_id: _this.data.userInfo.member_id,
      ser_id: _this.data.serverData.ser_id,
      all_no: _this.data.orderInfo.all_no,
      order_sn: _this.data.orderInfo.order_sn,
      // order_id: _this.data.orderInfo.order_id,
      getMsgType: "order"
    }
    _this.getMesList(chartParam);
  },
  getMesList: function(param) {
    const _this = this;
    server.postJSON("/index/chat/selectmsg", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      if (res.data.data.data && res.data.data.data.length > 0) {
        for (var i = 0; i < res.data.data.data.length; i++) {
          res.data.data.data[i].send_time = tool.formatDate(new Date(res.data.data.data[i].add_time * 1000), 'mm-dd');
        }
      }
      _this.setData({
        msgData: res.data.data
      });
      var query = wx.createSelectorQuery().in(_this);
      query.select('#scroll-container').boundingClientRect(function(res) {
        setTimeout(function() {
          _this.setData({
            scrollTop: res.height
          });
        }, 400);
      }).exec()
    });
  },
  navigateTo: function() {
    wx.navigateBack({
      delta: 5
    });
    wx.navigateTo({
      url: '/pages/container/goods-details/goods-details',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    //获得聊天组件
    this.chat = this.selectComponent("#chat");
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    
  },
  // 显示聊天界面
  showChat: function() {

    this.chat.showModal()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  applyRefund: function() {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    if (res.from == "button") {
      const dataset = res.target.dataset;
      return {
        path: "/pages/container/goods-details/goods-details?goodsId=" + dataset.goods_id + "&type=share&cantuanid=" + dataset.cantuanid,
        imageUrl: dataset.shareImg
      }
    } else {
      var goods_id = this.data.orderInfo.goods_list[0].item_id ? this.data.orderInfo.goods_list[0].item_id : this.data.orderInfo.goods_list[0].goods_id
      return {
        path: "pages/container/goods-details/goods-details?goodsId=" + goods_id,
        imageUrl: this.data.orderInfo.share_img
      }
    }
  }
})