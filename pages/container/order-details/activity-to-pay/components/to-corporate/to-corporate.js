// pages/container/order-details/activity-to-pay/components/to-corporate/to-corporate.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    serverInfo: {
      type: Object,
      require: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShow: false,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    show: function() {
      console.log(this)
      this.setData({
        isShow: true
      })
    },
    _show: function() {
      this.show()
    },
    hidden: function() {
      this.setData({
        isShow: false
      })
    },
    setClipboardData() {
      const _this = this;
      wx.setClipboardData({
        data: _this.properties.serverInfo.wechat_number
      })
    },
    stopMove: function(){
      
    }
  }
})
