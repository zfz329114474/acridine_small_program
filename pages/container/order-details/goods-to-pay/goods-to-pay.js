// pages/container/order-details/goods-to-pay/goods-to-pay.js
const app = getApp();
const tool = require("../../../../utils/util.js");
const server = require("../../../../utils/server.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isIPhoneX: false,
    countDownTime: null,
    pageShow: false,
    orderInfo: {},
    isWalletPay: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    _this.loadPage(options);
    this.setData({
      isIPhoneX: app.globalData.isIPhoneX,
      userInfo: wx.getStorageSync('userInfo')
    })
  },
  loadPage: function(param) {
    const _this = this;
    param.member_id = wx.getStorageSync('userInfo').member_id;
    wx.showLoading({
      title: '加载中',
    });
    server.postJSON("/index/order/order_info", param, function(res) {
      _this.setData({
        pageShow: true
      });
      wx.hideLoading();
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      let orderInfo = res.data.data;
      for (var j = 0; j < orderInfo.goods_list.length; j++) {
        if (orderInfo.goods_list[j].item_image) {
          orderInfo.goods_list[j].goods_image = orderInfo.goods_list[j].item_image
          orderInfo.goods_list[j].goods_num = orderInfo.goods_list[j].item_num
          orderInfo.goods_list[j].goods_name = orderInfo.goods_list[j].item_name
          orderInfo.goods_list[j].goods_price = orderInfo.goods_list[j].item_price
          orderInfo.goods_list[j].goods_spec = orderInfo.goods_list[j].goods_spec
        }
      }
      if (orderInfo.order_label == 1) {
        const goods_num = orderInfo.goods_list.reduce(function(prev, next) {
          return prev += next.goods_num;
        }, 0);
        orderInfo.order_list = [{
          goods_image: orderInfo.goods_list[0].goods_image,
          goods_num: goods_num,
          order_id: orderInfo.goods_list[0].order_id,
          goods_name: orderInfo.goods_list[0].goods_name,
          goods_spec: orderInfo.goods_list[0].goods_spec,
          order_label: orderInfo.order_label,
          goods_price: orderInfo.goods_list[0].goods_price,
          monthlist: orderInfo.goods_list
        }];
      } else {
        orderInfo.order_list = orderInfo.goods_list;
      }
      _this.setData({
        orderInfo: orderInfo
      });
      _this.countDownTime();
    })
  },
  countDownTime: function() {
    const _this = this;
    var hour = 0,
      minute = 0,
      second = 0,
      count_down_text = '';//时间默认值
    let countTime = _this.data.orderInfo.count_down;

    // countDownTime
    if(countTime > 0) {
      hour = tool.padLeftZero(parseInt(countTime / (60 * 60)) + '' );
      minute = tool.padLeftZero(parseInt((countTime - hour * 60) / 60) + '');
      second = tool.padLeftZero(countTime % 60 + '');
      countTime--;
      if(hour > 0) {
        count_down_text += hour + "天";
      }
      count_down_text += minute + "分钟" + second + "秒";
      this.setData({
        'orderInfo.count_down': countTime,
        'orderInfo.count_down_text': count_down_text + '立即支付'
      });

      _this.data.countDownTime = setTimeout(function () {
        _this.countDownTime();
      }, 1000);
    } else {
      _this.setData({
        'orderInfo.count_down_text': '订单失效',
        'orderInfo.order_state': 0
      });
      clearInterval(_this.data.countDownTime);
    }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  /**
   * 是否使用钱包支付
   */
  switchChange:function(e){
    const _this = this;
    var isWalletPay = e.detail.value;
    if (_this.data.orderInfo.available_predeposit <= 0) {
      isWalletPay = false;
    }
    this.setData({
      isWalletPay: isWalletPay
    })
  },
  // 调起支付
  topay: function() {
    const _this = this;
    if (_this.data.order_state == 0) {
      tool.warning('订单已失效');
      return;
    }
    const param = {
      member_id: _this.data.userInfo.member_id,
      address_id: _this.data.orderInfo.address.daddress_id,
      all_no: _this.data.orderInfo.all_no,
      order_label: _this.data.orderInfo.order_label,
      openid: _this.data.userInfo.openid
    }
    if (_this.data.isWalletPay) {
      param.is_wallet = 2;
      wx.showModal({
        title: '提示',
        content: '您将使用钱包支付',
        success: function (res) {
          if (res.confirm) {
            applyPay(param);
          } else {
            tool.warning("取消支付");
          }
        }
      });
    } else {
      param.is_wallet = 1;
      applyPay(param);
    } 
    function applyPay(param) {
      server.postJSON("/index/Wxpay/index", param, function (res) {
        if (res.data.error != 0) {
          tool.warning(res.data.message);
          return;
        }
        if (res.data.data.order_state == 20) {
          wx.switchTab({
            url: '/pages/container/order-list/order-list',
          });
          return;
        }
        wx.requestPayment({
          "timeStamp": res.data.data.timeStamp,
          "nonceStr": res.data.data.nonceStr,
          "package": res.data.data.package,
          "signType": 'MD5',
          "paySign": res.data.data.paySign,
          "success": function (r) {
            console.log(r)
            if (r.errMsg == "requestPayment:ok") {
              wx.navigateTo({
                url: '/pages/container/order-details/order-details?all_no=' + param.all_no + "&order_label=" + param.order_label
                // url: '/pages/container/order-list/order-list'
              })
            }
          },
          fail: function (res) {
            tool.warning("支付失败");
          }
        })
      });
    }

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})