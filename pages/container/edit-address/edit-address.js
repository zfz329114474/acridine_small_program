// pages/container/edit-address/edit-address.js
const server = require("../../../utils/server.js");
const app = getApp();
const tool = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    regionData: [],
    regionIndex: [0, 0, 0], 
    sendAddressInfo:{},
    addressInfo: {
      member_id: "", //会员id
      true_name: "", //联系人
      mob_phone: "", //电话
      province_id: "", //省份id 
      province_name: "", //省份名字 如福建省
      city_id: "", //如厦门市id
      city_name: "", //如厦门
      area_id: "", //如湖里区id
      area_name: "", //湖里区
      address: "", //如禾山街道xxx号xxx室
      area_info: "", // 如福建省 厦门市 湖里区
      is_default: 0
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    console.log(options);
    app.getLoadData().then(function(res) {
      if(res.error != 0) {
        tool.warning(res.message);
        return
      }
      options.member_id = res.data.member_id;
      _this.getAddressInfo(options);

      _this.setData({
        "fromPage": options.fromPage ? options.fromPage : false,
        "addressInfo.member_id": res.data.member_id
      });

    })
  },
  /**
   * 根据地址id获取地址信息
   */
  getAddressInfo: function(param) {
    const _this = this;
    server.postJSON("/index/user/getAddress", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        sendAddressInfo: res.data.data,
        addressInfo: res.data.data
      })
    });
    this.loadarea();
  },
  /**
   * 提交地址保存
   */
  editAddress: function() {
    const _this = this;
    const addressInfo = _this.data.sendAddressInfo;
    if (tool.trim(addressInfo.true_name) == "") {
      tool.warning("请填写联系人");
      return;
    }
    if (!tool.isValidMobile(addressInfo.mob_phone)) {
      tool.warning("手机号码不符合规则");
      return;
    }
    if (!_this.data.addressInfo.area_info) {
      tool.warning("请选择省市区");
      return;
    }
    if (tool.trim(addressInfo.address) == "") {
      tool.warning("请填写详细地址");
      return;
    }
    
    server.postJSON("/index/user/setAddress", addressInfo, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      let delta = 1;
      if (_this.data.fromPage == "confirm") {
        delta = 2;
        app.globalData.changeAddress = {
          isChange: true,
          addressInfo: addressInfo
        }
      } else {
        const pages = getCurrentPages();
          console.log(pages)
        for (var i = 0; i < pages.length; i++) {
          if (pages[i].route == 'pages/container/my-address-list/my-address-list') {
            pages[i].loadPage()
          }
        }
      }

      wx.navigateBack({
        delta: delta,

      })
    })
  },
  setIsDefault: function() {
    const _this = this;
    _this.setData({
      "addressInfo.is_default": _this.data.addressInfo.is_default == 0 ? 1 : 0
    })
  },
  // 加载省市区三联动
  loadarea: function() {
    const _this = this;
    server.postJSON("/index/user/getarea", function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        "addressInfo.province_name": res.data.data[0].area_name,
        "addressInfo.province_id": res.data.data[0].area_id,
        "regionData[0]": res.data.data
      });
      const param = {
        level: 2,
        province_id: res.data.data[0].area_id
      }
      _this.loadCitys(param);

    })
  },
  loadCitys: function(param) {
    const _this = this;
    server.postJSON("/index/user/get_citys", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        "addressInfo.city_name": res.data.data[0].area_name,
        "addressInfo.city_id": res.data.data[0].area_id,
        "regionData[1]": res.data.data
      });
      const cityParam = {
        level: 3,
        city_id: res.data.data[0].area_id
      }
      _this.loadDistrict(cityParam);
    })
  },
  loadDistrict: function(param) {
    const _this = this;
    server.postJSON("/index/user/get_district", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      if(res.data.data && res.data.data.length > 0) {
        _this.setData({
          "addressInfo.area_name": res.data.data[0].area_name,
          "addressInfo.area_id": res.data.data[0].area_id,
          "regionData[2]": res.data.data
        });
      } else {
        _this.setData({
          "addressInfo.area_name": '',
          "addressInfo.area_id": '',
          "regionData[2]": []
        });
      }

    })
  },
  /**
   * 选择地址弹窗
   */
  bindRegionChange: function(evt) {
    console.log(evt)
    const column = evt.detail.column;
    const value = evt.detail.value;
    const _this = this;
    let param = {};
    switch (column) {
      case 0:
        param = {
          level: 2,
          province_id: _this.data.regionData[0][value].area_id
        }
        _this.setData({
          "isChangeAddress": true,
          "regionIndex[0]": value,
          "addressInfo.province_id": _this.data.regionData[0][value].area_id,
          "addressInfo.province_name": _this.data.regionData[0][value].area_name
        })
        _this.loadCitys(param);
        break;
      case 1:
        param = {
          level: 3,
          city_id: _this.data.regionData[1][value].area_id
        }
        _this.setData({
          "isChangeAddress": true,
          "regionIndex[1]": value,
          "addressInfo.city_id": _this.data.regionData[1][value].area_id,
          "addressInfo.city_name": _this.data.regionData[1][value].area_name
        })
        _this.loadDistrict(param);
        break;
      case 2:
        _this.setData({
          "isChangeAddress": true,
          "regionIndex[2]": value,
          "addressInfo.area_id": _this.data.regionData[2][value].area_id,
          "addressInfo.area_name": _this.data.regionData[2][value].area_name
        })
        break;
    }
  },
  confirmAddressHandle: function(evt) {
    const _this = this;
    _this.setData({
      "addressInfo.area_info": _this.data.addressInfo.province_name + " " + _this.data.addressInfo.city_name + " " + _this.data.addressInfo.area_name
    })
    _this.setData({
      sendAddressInfo: _this.data.addressInfo
    })
  },
  /**
   * 用户进行填写
   */
  changInput: function(evt) {
    tool.changInput(evt,this);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})