// pages/container/customization/customization.js
const app = getApp();
const tool = require("../../../utils/util.js");
const server = require("../../../utils/server.js");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    isMake: false,
    userInfo: {},
    isShowExplain: false,
    memberInfo: {},
    showLoginType:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    _this.loadPage();
    _this.setData({
      userInfo: wx.getStorageSync('userInfo')
    });
  },
  stopPropagation() {

  },
  // 加载私人定制 介绍图片
  loadPage: function() {
    const _this = this;
    server.postJSON("/index/goods/customized_info",function(res){
      if(res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        customData: res.data.data
      });
    });
  },
  showChat: function() {
    const _this = this;
    if (!app.isMember(_this.data.userInfo)) {
      _this.setData({
        showLoginType: 'showChat'
      });
      _this.loginView.showModel();
      return;
    }
    wx.navigateTo({
      url: '/pages/container/chat/chat?ser_id=' + _this.data.customData.ser_info.ser_id,
    });
    return;
    const param = {
      getMsgType: "goods",
      goods_label: 4,
      member_id: app.globalData.userInfo.member_id,
      ser_id: _this.data.customData.ser_info.ser_id,
      goods_id: 'abcd'
    }
    server.postJSON("/index/chat/selectmsg", param, function (res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      for (var i = 0; i < res.data.data.data.length; i++) {
        res.data.data.data[i].send_time = tool.formatDate(new Date(res.data.data.data[i].add_time * 1000), 'mm-dd hh:ii:ss')
      }
      _this.setData({
        msgData: res.data.data
      })
      _this.chat.showModal();
    })
  },
  // 登陆成功
  _loadSuccess: function (evt) {
    this.setData({
      userInfo: evt.detail.userInfo
    });
    wx.setStorage({
      key: 'userInfo',
      data: evt.detail.userInfo
    });
    if (this.data.showLoginType == 'showChat') {
      this.showChat();
    } else {
      this.goMake();
    }
  },
  _resetMsg: function (evt) {
    console.log(evt);
    this.showChat();
  },
  goMake: function() {
    const _this = this;
    if (!app.isMember(_this.data.userInfo)) {
      _this.setData({
        showLoginType: 'goMake'
      });
      _this.loginView.showModel();
      return;
    }
    _this.setData({
      isMake: true
    })
  },
  _showLoginEvent: function(evt) {  

  },
  hiddenMakeRelation: function() {
    this.setData({
      isMake: false
    })
  },
  showCustomExplain: function() {
    this.setData({
      isShowExplain: true
    });
  },
  hiddenCustomExplain: function() {
    this.setData({
      isShowExplain: false
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    //获取聊天组件
    this.loginView = this.selectComponent("#login-view");
    this.chat = this.selectComponent("#chat");
  },
  // 联系人更改
  changeinput: function(evt) {
    tool.changInput(evt,this);
  },
  createServiceOrder: function() {
    const _this = this;
    let memberInfo = this.data.memberInfo;
    if (!memberInfo.member_true_name) {
      tool.warning("联系人姓名不能为空");
      return;
    }
    if (!memberInfo.member_mobile) {
      tool.warning("联系方式不能为空");
      return;
    }
    if (!tool.isValidMobile(memberInfo.member_mobile)) {
      tool.warning("手机号格式不正确");
      return;
    }
    memberInfo.member_id = app.globalData.userInfo.member_id;
    memberInfo.openid = app.globalData.userInfo.openid;
    server.postJSON("/index/buy/customized", memberInfo, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      tool.success("等待客服联系。");
      _this.hiddenMakeRelation()
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    
  }
})