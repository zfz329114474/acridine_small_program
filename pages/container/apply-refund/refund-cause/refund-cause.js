// pages/container/apply-refund/refund-cause/refund-cause.js
const app = getApp();
const tool = require("../../../../utils/util.js");
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    causeList: Array,
  },

  /**
   * 组件的初始数据
   */
  data: {
    isHidden: true
  },

  /**
   * 组件的方法列表
   */
  methods: {
    _showModal: function(){
      this.setData({
        isHidden: false
      });
    },
    _hiddenModal: function(){
      this.setData({
        isHidden: true
      });
    },
    _chooseRefundCause: function(e){

      const reason_id = tool.getDataset(e).reason_id;
      console.log(reason_id)
      this.triggerEvent("chooseRefundCause", { reason_id: reason_id })
    },
    showModal: function(){
      this._showModal();
    }
  }
})
