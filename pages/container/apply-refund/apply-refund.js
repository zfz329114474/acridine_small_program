// pages/container/apply-refund/apply-refund.js
const app = getApp();
const tool = require("../../../utils/util.js");
const config = require("../../../config.js");
const server = require('../../../utils/server.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    maxImageLength: 4,
    refund:{
      causeDetails: "",
      cause: "",
      tempFiles: [],
      pic_arr: [],
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadReasonList();
    this.setData({
      "param": {
        order_id: options.order_id,
        order_label: options.order_label,
        member_id: wx.getStorageSync('userInfo').member_id
      }
    })
  },
  //删除选中的图片
  delSelectImg:function(evt){
    const index = tool.getDataset(evt).index;
    let tempFiles = this.data.refund.tempFiles;
    tempFiles.splice(index, 1)
    const maxImageLength = this.data.maxImageLength + 1;

    this.setData({
      maxImageLength: maxImageLength,
      "refund.tempFiles": tempFiles
    });
  },
  showRefundCause: function () {
    this.refundCause.showModal();
  },

  loadReasonList: function() {
    const _this = this; 
    server.postJSON("/index/order/reason_list", function(res) {
      if(res.data.error == 0) {
        _this.setData({
          causeList: res.data.data
        })
      }
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 获取退款原因组件
    this.refundCause = this.selectComponent("#refund-cause");
  },
  _chooseRefundCause: function(e){
    const reason_id = tool.getComponentDetail(e).reason_id
    const causeList = this.data.causeList; 
    const causeItem =  causeList.filter(function(item){
      if (item.reason_id === reason_id) {
        return item;
      }
    });
    this.setData({
      "refund.cause": causeItem[0]
    })
  },
  inputChange: function(evt){
    const changeItem = tool.inputChange(evt, this);
    tool.changInput(evt,this);
  },
  addImg: function(e){
    const _this = this;
    let maxImageLength = _this.data.maxImageLength;
    wx.chooseImage({
      count: maxImageLength,
      success: function(res){
        if (res.errMsg == "chooseImage:ok"){
          console.log(res)
          for (var i = 0; i < res.tempFilePaths.length; i++) {
            wx.uploadFile({
              url: config.baseApiUrl + '/order/upload',
              filePath: res.tempFilePaths[i],
              name: 'file',
              formData: {'img_path': res.tempFilePaths[i]},
              success:function(resp){
                resp.data = JSON.parse(resp.data);
                maxImageLength = _this.data.maxImageLength;
                if (resp.data.error == 0) {
                  let tempFiles = _this.data.refund.tempFiles;
                  tempFiles.push(resp.data.data);
                  console.log(tempFiles)
                  _this.setData({
                    maxImageLength: --maxImageLength,
                    "refund.tempFiles": tempFiles,
                  })
                }
              }
            })

          }
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },
  /**
   * 退款申请
   */
  order_refund: function() {
    const _this = this;
    let param = _this.data.param;
    let pic_info = _this.data.refund.tempFiles;
    pic_info = pic_info.reduce(function (total, next){
      total.push(next.filePath);
      return total;
    },[]).join(',');
    param.reason_id = _this.data.refund.cause.reason_id;
    param.reason_info = _this.data.refund.cause.reason_info;
    param.pic_info = pic_info;
    param.buyer_message = _this.data.refund.causeDetails;
    if(!param.reason_id) {
      tool.warning('请选择退款原因');
      return;
    }
    server.postJSON('/index/order/order_refund',param,function(res){
      if(res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      tool.success("已提交");
      var pages = getCurrentPages() 
      console.log(pages );
      var orderDetailsPage = pages.filter(function(item) {
        if (item.route == "pages/container/order-details/order-details" || item.route == "pages/container/order-details/activity-payed/activity-payed") {
          return item;
        }
      })[0];
      console.log(orderDetailsPage)

      setTimeout(function(){
        wx.navigateBack({
          delta: 1,
          success() {
            orderDetailsPage.loadPage(orderDetailsPage.options)
          }
        });
      },1500);
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})