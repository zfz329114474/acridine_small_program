// pages/container/remark/remark.js
const tool = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const msgKey = options.msgKey;
    this.setData({
      msgKey: msgKey
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  _inputChange: function(e){
    tool.changInput(e,this);
  },
  submit: function() {
    const _this = this;
    const pages = getCurrentPages();
    const msgKey = _this.data.msgKey;
    const prevPage = pages[pages.length -2];
    console.log(pages)
    wx.navigateBack({
      delta:1,
      success:function(){
        if (_this.data.message) {
          prevPage.setData({
            [msgKey]: _this.data.message
          });
        }
      }
    });

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})