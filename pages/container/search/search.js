// pages/container/search/search.js
const server = require("../../../utils/server.js");
const app = getApp();
const tool = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchText: "",
    goodsList: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  /**
   * 清除搜索框
   */
  clearSearchText: function() {
    this.setData({
      searchText: ""
    })
  },
  searchTextInput:function(evt){
    this.setData({
      searchText: evt.detail.value
    })
  },
  /**
   * 搜索
   */
  searchHandle: function() {
    const _this = this;
    console.log(_this.data.searchText)
    const searchText = _this.data.searchText;
    if (!searchText) {
      tool.warning("请输入关键词");
      return;
    }
    const param = {
      text: searchText
    }
    server.postJSON("/index/goods/query", param, function(res){
      if(res.data.error !== 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        goodsList: res.data.data
      });
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})