// pages/container/my-wallet/my-wallet.js
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageShow: false,
    isShowRecharge: false,
    userInfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: wx.getStorageSync('userInfo')
    })
    const _this = this;
    _this.loadPage();
    tool.getMemberInfo(_this);
  },
  loadPage:function() {
    const _this = this;
    const param = {
      member_id: wx.getStorageSync('userInfo').member_id
    };
    server.postJSON("/index/user/wallet_log",param, function(res){
      _this.setData({
        pageShow: true
      })
      if(res.data.error != 0 ) {
        tool.warning(res.data.message);
        return;
      }
      for(var i = 0; i < res.data.data.length; i++) {
        if (res.data.data[i].operation_type == 1 || res.data.data[i].operation_type == 4) {
          res.data.data[i].add_or_minus = "+";
        } else if (res.data.data[i].operation_type == 2 || res.data.data[i].operation_type == 3) {
          res.data.data[i].add_or_minus = "-";
        }
        res.data.data[i].create_time = tool.formatDate((new Date(res.data.data[i].add_time * 1000 )), "yyyy-mm-dd hh:ii");
      }
    _this.setData({
      wallet_list: res.data.data
    })
    });
  },
  stopPropagation: function(){

  },
  changInput: function(evt) {
    tool.changInput(evt, this);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 显示充值卡model 
   */
  showRecharge: function () {
    this.setData({
      isShowRecharge: true
    })
  },
  /**
   * 隐藏充值弹窗
   */
  hiddenRecharge: function() {
    this.setData({
      isShowRecharge: false
    })
  },
  /**
   * 确认充值
   */
  confirmRecharge: function() {
    const _this = this; 

    if (!_this.data.send || _this.data.send.password == '') {
      tool.warning("充值码不能为空");
      return;
    }
    const param = {
      member_id: _this.data.userInfo.member_id,
      password: _this.data.send.password
    };
    server.postJSON("/index/user/recharge", param, function (res) {
      if(res.data.error != 0 ){
        tool.warning(res.data.message);
        return;
      }
      _this.loadPage();
      tool.getMemberInfo(_this);
      _this.hiddenRecharge(); 

    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})