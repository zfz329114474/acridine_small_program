// pages/container/goods-details/special/special.js
const app = getApp();
const server = require("../../../../utils/server.js");
const tool = require("../../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadPage(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  loadPage:function(param){
    const _this = this;
    wx.showNavigationBarLoading(); 
    server.postJSON("/index/goods/special",param,function(res){
      wx.hideNavigationBarLoading();
      if(res.data.error != 0){
        tool.warning(res.data.message);
      }
      _this.setData({
        goodsList: res.data.data
      })
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})