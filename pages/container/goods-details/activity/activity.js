// pages/container/goods-details/activity/activity.js
const app = getApp();
const server = require("../../../../utils/server.js");
const tool = require("../../../../utils/util.js");
const WxParse = require("../../../../wxParse/wxParse.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowModel: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    _this.setData({
      userInfo: wx.getStorageSync('userInfo')
    })
    _this.loadPage(options);
    _this.loadCompanyInfo();

  },
  loadPage: function(option) {
    const _this = this;
    const param = {
      member_id: wx.getStorageSync('userInfo').member_id,
      ua_id: option.uaId
    };
    server.postJSON('/index/groupbuy/world_kitchen', param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      res.data.data.item_num = 1;
      res.data.data.unit_price = _this.getUnitPrice(res.data.data);
      _this.setData({
        goodsData: res.data.data
      });
      //计算总价
      _this.getTotalPrice();
      //获取客服信息
      _this.getServerData({ ser_id: res.data.data.ser_id });
      WxParse.wxParse('article', 'html', res.data.data.ua_mobile_content, _this);
    });
  },

  // 获取客服信息
  getServerData: function (options) {
    const _this = this;
    server.postJSON("/index/chat/ser_info", options, function (res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        serverData: res.data.data
      });
    })
  },
  showChat: function () {
    const _this = this;
    if (!app.isMember(_this.data.userInfo)) {
      _this.setData({
        showLoginType: 'showChat'
      });
      _this.loginView.showModel();
      return;
    }
    const param = {
      getMsgType: "goods",
      member_id: app.globalData.userInfo.member_id,
      ser_id: 1,
      goods_id: this.data.goodsData.ua_id
    }
    server.postJSON("/index/chat/selectmsg", param, function (res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        msgData: res.data.data
      })
      _this.chat.showModal();
    })
  },
  // 重置消息
  _resetMsg: function () {
    this.showChat();
  },
  // 加载公司简介
  loadCompanyInfo: function() {
    var _this = this;
    server.postJSON("/index/index/footer", function(res) {
      if (res.data.error !== 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        companyInfo: res.data.data[0]
      })
    })
  },
  /**
   * 计算总价格
   */
  getTotalPrice: function() {
    const _this = this;
    const goodsData = _this.data.goodsData;
    const item_num = goodsData.item_num;
    let total_price = 0;
    if (goodsData.ua_type_id == 5) {
      total_price = (item_num - 1) * goodsData.unit_price
    } else {
      total_price = item_num  * goodsData.unit_price
    }
    this.setData({
      'goodsData.total_price': total_price
    });
  },
  /**
   * 根据类别获取单价
   */
  getUnitPrice: function(goods) {
    if (goods.ua_type_id == 5) {
      return goods.ua_guset_price
    } else {
      return goods.ua_price
    }
  },
  // 登陆成功后 存储数据
  _loadSuccess: function (evt) {
    this.setData({
      userInfo: evt.detail.userInfo
    });
    wx.setStorage({
      key: 'userInfo',
      data: evt.detail.userInfo
    });
    if (this.data.showLoginType == 'showChat') {
      this.showChat();
    } else {
      this.showOption();
    }
  },
  /**
   * 显示 弹窗
   */
  showOption: function() {
    const _this = this;
    if (!app.isMember(_this.data.userInfo)) {
      _this.loginView.showModel();
      return;
    }
    if (_this.data.userInfo.is_wkmember != 1 && _this.data.goodsData.ua_type_id == 5) {
      tool.warning('只允许世界会员参加');
      return;
    }
    this.setData({
      isShowModel: true
    });
  },
  /**
   * 隐藏弹窗
   */
  hiddenOption: function() {
    this.setData({
      isShowModel: false
    });
  },
  /**
   * 参加人数减 
   */
  minusJoinNum: function(e) {
    const _this = this;
    let item_num = this.data.goodsData.item_num;
    if (item_num <= 1) {
      tool.warning('不能再少了');
      return;
    }
    item_num = item_num - 1;
    _this.setData({
      'goodsData.item_num': item_num
    })
    _this.getTotalPrice();
  },
  addJoinNum: function() {
    const _this = this;
    const goodsData = _this.data.goodsData;
    const ua_guset_count = goodsData.ua_guset_count + 1; //允许世界厨房携带人数
    const ua_limit_count = goodsData.ua_limit_count; //允许世界厨房携带人数
    const ua_remained = goodsData.ua_remained // 活动剩余名额
    // 计算剩余可添加人数 
    // 1、世界厨房需判断 允许携带人数
    // 2、其他活动只需要 判断剩余数量
    let limitNum = 0;
    let msg = '报名人数超出剩余席位或单团体人数限制';
    let item_num = goodsData.item_num;
    if (goodsData.ua_type_id == 5) {
      if (goodsData.ua_member_only == 1) {
        limitNum = 1;
        msg = '只允许会员本人参加';
      } else {
        limitNum = ua_guset_count > ua_remained ? ua_remained : ua_guset_count;
      }
    } else {
      limitNum = ua_limit_count > ua_remained ? ua_remained : ua_limit_count;
    } 
    if (limitNum > item_num) {
      item_num++;
      _this.setData({
        'goodsData.item_num': item_num
      });
      _this.getTotalPrice();
    } else {
      tool.warning(msg);
    }
  },
  /**
   * 确认订单
   */
  toConfirmOrder: function() {
    const _this = this;
    const activityData = _this.data.goodsData;
    const goodsData = {
      ua_face: activityData.ua_face,
      ua_title: activityData.ua_title,
      ua_start_time: tool.formatDate(new Date(activityData.ua_start_time * 1000), 'yyyy-mm-dd')
    };
    let order_label;
    if (activityData.ua_type_id == 5) {
      order_label = 5;
    } else if ( activityData.ua_type_id == 11) {
      order_label = 6;
    } else if (activityData.ua_type_id == 12) {
      order_label = 7;
    }
    let confirmData = {
      order_label: order_label,
      ua_type_id: activityData.ua_type_id,
      ua_id: activityData.ua_id,
      activityData: goodsData,
      item_num: activityData.item_num,
      unit_price: activityData.unit_price,
      total_price: activityData.total_price
    }
    confirmData = JSON.stringify(confirmData);
    wx.navigateTo({
      url: '/pages/container/confirm-order-details/activity-confirm-order/activity-confirm-order?confirmData=' + confirmData
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.chat = this.selectComponent("#chat");
    //获得登陆组件
    this.loginView = this.selectComponent("#login-view");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})