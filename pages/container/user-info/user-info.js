// pages/container/user-info/user-info.js
const app = getApp();
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showModal: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    _this.setData({
      userInfo: wx.getStorageSync('userInfo')
    });
    _this.setEndDay();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.loginView = this.selectComponent("#login-view");
    console.log(this)
  },
  amandBirthday: function() {

  },
  _loadSuccess:function(userInfo){
    this.setData({
      userInfo: userInfo
    });
    wx.setStorage({
      key: 'userInfo',
      data: userInfo
    })
  },
  // 获取当前时间 设置成最大日期
  setEndDay: function () {
    let timestamp = new Date();
    this.setData({
      endDay: tool.formatDate(timestamp, "yyyy-mm-dd")
    })
  },
  // 更改用户头像
  remandUserPic: function() {
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function(res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths
      }
    })
  },
  // 设置生日
  changeBirthdayHandle: function(evt) {
    const _this = this;
    const param = {
      member_id: app.globalData.userInfo.member_id,
      type: "生日",
      "member_birthday": evt.detail.value
    }
    server.postJSON("/index/user/setMember", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      app.globalData.userInfo.member_birthday = evt.detail.value;
      _this.setData({
        "userInfo.member_birthday": evt.detail.value
      });
    });
  },
  // 弹出更改姓名弹窗
  changeName: function() {
    this.setData({
      isShowModal:true
    });
  },
  // 关闭弹窗
  closeDialog: function() {
    this.setData({
      isShowModal: false
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    
  },
  showBingPhone: function(){
    this.loginView.showModel();
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})