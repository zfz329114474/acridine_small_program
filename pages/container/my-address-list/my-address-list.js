// pages/container/my-address-list/my-address-list.js
const app = getApp();
const tool = require("../../../utils/util.js");
const server = require("../../../utils/server.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{},
    addressList: [],
    fromPage: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    app.getLoadData().then(function(res){
      _this.setData({
        userInfo: res.data
      })
      if (options.fromPage) {
        _this.setData({
          fromPage: options.fromPage
        });
      }
      _this.loadPage()
    })
  },
  loadPage: function() {
    const _this = this;
    const param = {
      member_id: _this.data.userInfo.member_id
    };
    server.postJSON("/index/user/addressList", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return
      }
      _this.setData({
        addressList: res.data.data
      })
    })
  },
  /**
   * 删除地址
   */
  deleteAddress: function(evt) {
    const _this = this;
    const address_id = tool.getDataset(evt).addressId;
    const param = {
      member_id: app.globalData.userInfo.member_id,
      address_id: address_id
    }
    wx.showModal({
      title: '提示',
      content: '确定要删除该地址吗？',
      success: function(res) {
        if (res.confirm) {
          server.postJSON("/index/user/delAddress", param, function(res) {
            if (res.data.error != 0) {
              tool.warning(res.data.message);
              return;
            }
            _this.loadPage()
          });

        }
      }
    })
  },
  setDefaultAddress (evt) {
    const _this = this;
    const address_id = tool.getDataset(evt).addressId;
    console.log(address_id)
    let addressList = _this.data.addressList;
    const defaultAddress = addressList.filter(function(item){
      if (item.is_default == 1) {
        return item;
      }
    })[0];
    if (defaultAddress && defaultAddress.address_id == address_id) {
      return
    }
    console.log(defaultAddress)
    let sendAddress = addressList.filter(function (item) {
      if (item.address_id == address_id) {
        return item;
      }
    })[0];
    sendAddress.is_default = 1;
    server.postJSON("/index/user/setAddress",sendAddress,function(res) {
      if(res.data.error != 0) {
        tool.warning(res.data.message);
        return
      }
      for(var i = 0; i < addressList.length; i++) {
        if(addressList[i].address_id == address_id) {
          addressList[i].is_default = 1;
        } else {
          addressList[i].is_default = 0;
        }
      }
      _this.setData({
        addressList: addressList
      })
    })
  },
  navigateTo: function(evt) {
    const _this = this;
    const from = _this.data.fromPage;
    let url = tool.getDataset(evt).url;
    if (from == "confirm") {
      url += "&fromPage=confirm";
    }
    wx.navigateTo({
      url: url,
      success:function(){
      }
    });
  },
  /**
   * 点击地址判断是否要跳转
   * 
   * 
   */
  _selectAddress: function(evt) {
    const _this = this;
    const fromPage = _this.data.fromPage;
    const addressId = tool.getDataset(evt).addressId;
    
    if (fromPage == "confirm") {
      console.log("fromPage:" + fromPage)  
      const addressInfo = _this.data.addressList.filter(function(item) {
        if (item.address_id == addressId) {
          return item;
        }
      })[0];
      console.log()
      app.globalData.changeAddress = {
        isChange: true,
        addressInfo: addressInfo
      }
      wx.navigateBack({
        delta: 1
      });
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (app.globalData.addAddress) {
      this.loadPage()
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})