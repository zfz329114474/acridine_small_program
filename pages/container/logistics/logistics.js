// pages/container/logistics/logistics.js
const server = require('../../../utils/server.js');
const tool = require('../../../utils/util.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageShow: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadPage(options);
  },
  loadPage: function (param){
    console.log(param)
    const _this = this;
    wx.showLoading({
      title: '加载中'
    })
    server.postJSON("/index/Logistics/Logistics_info",param,function(res){
      wx.hideLoading();
        // if(res.data.status != 0 ) {
        //   tool.warning(res.data.msg);
        //   return;
        // }
        _this.setData({
          pageShow: true,
          data: res.data.result
        })
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})