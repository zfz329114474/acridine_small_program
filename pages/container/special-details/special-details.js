// pages/container/special-details/special-details.js
const app = getApp();
const WxParse = require("../../../wxParse/wxParse.js");
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const ck_id = options.ckId;
    this.loadPage(ck_id);
  },
  loadPage: function(ck_id) {
    const _this = this;
    let param = {
      ck_id: ck_id
    }
    server.postJSON("/index/cook/cook_story", param, function(res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      if (res.data.data.goods_cook && res.data.data.goods_cook.length > 0) {
        for (var i = 0; i < res.data.data.goods_cook.length; i++) {
          res.data.data.goods_cook[i].jump_url = '/pages/container/goods-details/goods-details?goodsId=' + res.data.data.goods_cook[i].goods_id;
        }
      }
      _this.setData({
        cookData: res.data.data
      });
      // WxParse.wxParse('article', 'html', ces, _this);
      WxParse.wxParse('article', 'html', res.data.data.cs_mobile_content, _this);
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})