// pages/container/chat/chat.js
const app = getApp();
const server = require('../../../utils/server.js');
const tool = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chat_BG: app.globalData.chat_BG
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getServerInfo(options);
  },
  // 获取客服信息
  getServerInfo: function (ser_id) {
    const _this = this;
    server.postJSON("/index/chat/ser_info",ser_id,function(res) {
      if(res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      _this.setData({
        ser_info: res.data.data
      });

      _this.showChat();
    })
  },
  // 获取聊天内容
  showChat: function() {
    const _this = this;
    const param = {
      getMsgType: "goods",
      goods_label: 4,
      member_id: app.globalData.userInfo.member_id,
      ser_id: _this.data.ser_info.ser_id,
      goods_id: 'abcd'
    }
    server.postJSON("/index/chat/selectmsg", param, function (res) {
      if (res.data.error != 0) {
        tool.warning(res.data.message);
        return;
      }
      for (var i = 0; i < res.data.data.data.length; i++) {
        res.data.data.data[i].send_time = tool.formatDate(new Date(res.data.data.data[i].add_time * 1000), 'mm-dd hh:ii:ss')
      }
      _this.setData({
        msgData: res.data.data
      })
      _this.chat.showModal();
    })
  },
  _resetMsg: function (evt) {
    console.log(evt);
    this.showChat();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.chat = this.selectComponent("#chat");
  },

  _resetMsg: function (evt) {
    this.showChat();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})