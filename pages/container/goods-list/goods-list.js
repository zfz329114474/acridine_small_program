// pages/container/goods-list/goods-list.js
const app = getApp();
const server = require("../../../utils/server.js");
const tool = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const _this = this;
    _this.loadpageData(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  // 加载列表
  loadpageData: function(param){
    const _this = this;
    server.postJSON("/index/goods/typeInfo",param,function(res){
      if(res.data.error !== 0) {
        tool.warning(res.data.message);
        return;
      }
      for(var i = 0; i < res.data.data.length; i++) {
        if (res.data.data[i].goods_label == 1) {
          res.data.data[i].goods_label_text == '月定制';
        } else if(res.data.data[i].goods_label == 2) {
          res.data.data[i].goods_label_text == '拼团';
        } else if (res.data.data[i].goods_label == 3) {
          res.data.data[i].goods_label_text == '众筹';
        }
      }
      _this.setData({
        goodsList: res.data.data
      })
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})