// pages/container/order-list/order-list.js
const app = getApp();
const tool = require("../../../utils/util.js");
const server = require("../../../utils/server.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageShow: false,
    no_order_bg_height: wx.getSystemInfoSync().windowHeight + "px",
    orderList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const _this = this;
    // const member_id = app.globalData.userInfo.member_id;
    
  },
  delOrder: function(evt) {
    const _this = this;
    const param = {
      all_no: evt.target.dataset.all_no
    };

    wx.showModal({
      title: '警告',
      content: '删除后不可恢复',
      success: function(res) {
        if (res.confirm) {
          server.postJSON("/index/order/del", param, function(res) {
            // if(res.data.data.error == 0 ){
            _this.loadPage();
            // }
          });
        }
      }
    })
  },
  loadPage: function() {
    const _this = this;
    const param = {
      member_id: wx.getStorageSync('userInfo').member_id
    }
    wx.showLoading({
      title: '加载中',
    })
    server.postJSON("/index/order/order_list", param, function(res) {
      _this.setData({
        pageShow: true
      });
      wx.hideLoading();
      wx.stopPullDownRefresh();
      if (res.data.error !== 0) {
        tool.warning(res.data.message);
        return;
      }
      let orderList = res.data.data;
      // 重新拼装orderList
      for (var i = 0; i < orderList.length; i++) {
        // 整理月定制订单，只展示一条
        if (orderList[i][0].order_label == 1) {
          if (orderList[i][0].order_state == 10) {
            orderList[i] = [
              orderList[i][0]
            ];
          } else if (orderList[i][0].order_state == 20) {
            orderList[i][0].month_ship_text = orderList[i][0].month + "月待发货";
            orderList[i] = [
              orderList[i][0]
            ];
          }
        }
        for (var j = 0; j < orderList[i].length; j++) {
          var orderItem = orderList[i][j];
          if (orderItem.order_label == 1) {
            orderItem.order_label_text = "月定制";
          } else if (orderItem.order_label == 2) {
            orderItem.order_label_text = "拼团";
          } else if (orderItem.order_label == 3) {
            orderItem.order_label_text = "众筹";
          }
          if (orderItem.order_state == 10) {
            orderItem.order_state_text = "去支付";
            orderItem.cancel_state_text = "取消订单";
            // 处理整理跳转路径
            if (orderItem.order_label == 0 || orderItem.order_label == 1 || orderItem.order_label == 2 || orderItem.order_label == 3) {
              orderItem.url = "/pages/container/order-details/goods-to-pay/goods-to-pay?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 4){
              orderItem.url = "/pages/container/order-details/activity-payed/activity-payed?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else {
              orderItem.url = "/pages/container/order-details/activity-to-pay/activity-to-pay?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
          } else if (orderItem.order_state == 0) {
            if (orderItem.order_label == 0 || orderItem.order_label == 1 || orderItem.order_label == 2 || orderItem.order_label == 3) {
              orderItem.url = "/pages/container/order-details/goods-to-pay/goods-to-pay?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 4) {
              orderItem.url = "/pages/container/order-details/activity-payed/activity-payed?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }else {
              orderItem.url = "/pages/container/order-details/activity-to-pay/activity-to-pay?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
            orderItem.order_state_text = '已取消';
          }  else if (orderItem.order_state == 20) {
            //展示标签 处理跳转路径  0普通商品 1月定制 2拼团 3众筹 4私人定制 5世界厨房 6官方组局 7美食旅行
            if (orderItem.order_label == 0) {
              if (orderItem.refund_state == 3) {
                orderItem.order_state_text = '退款中';
              } else if ([1, 2, 4].indexOf(orderItem.refund_state) > 0) {
                orderItem.order_state_text = '已退款';
              } else {
                orderItem.order_state_text = "已付款";
              }
              orderItem.url = "/pages/container/order-details/order-details?order_id=" + orderItem.order_id + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 1) {
              if (orderItem.refund_state == 3) {
                orderItem.order_state_text = '退款中';
              } else if ([1, 2, 4].indexOf(orderItem.refund_state) > 0) {
                orderItem.order_state_text = '已退款';
              } else {
                orderItem.order_state_text = "已付款";
              }
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 2) {
              if (orderItem.refund_state == 3) {
                orderItem.order_state_text = '退款中';
              } else if ([1, 2, 4].indexOf(orderItem.refund_state) > 0) {
                orderItem.order_state_text = '已退款';
              } else {
                orderItem.order_state_text = "拼团中";
              }
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 3) {
              if (orderItem.refund_state == 3) {
                orderItem.order_state_text = '退款中';
              } else if ([1, 2, 4].indexOf(orderItem.refund_state) > 0) {
                orderItem.order_state_text = '已退款';
              } else {
                orderItem.order_state_text = "众筹中";
              }
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 4) {
              orderItem.url = "/pages/container/order-details/activity-payed/activity-payed?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 5 || orderItem.order_label == 6 || orderItem.order_label == 7 ) {
              if (orderItem.refund_state == 3) {
                orderItem.order_state_text = '退款中';
              } else if ([1, 2, 4].indexOf(orderItem.refund_state) > 0) {
                orderItem.order_state_text = '已退款';
              } else {
                orderItem.order_state_text = "待参加";
              }
              orderItem.url = "/pages/container/order-details/activity-payed/activity-payed?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
          } else if (orderItem.order_state == 30) {
            //展示标签 处理跳转路径  0普通商品 1月定制 2拼团 3众筹 4私人定制 5世界厨房 6官方组局 7美食旅行
            if (orderItem.order_label == 0) {
              orderItem.url = "/pages/container/order-details/order-details?order_id=" + orderItem.order_id + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 1) {
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 2) {
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 3) {
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
            orderItem.order_state_text = '已发货';
          } else if (orderItem.order_state == 40) {
            //展示标签 处理跳转路径  0普通商品 1月定制 2拼团 3众筹 4私人定制 5世界厨房 6官方组局 7美食旅行
            if (orderItem.order_label == 0) {
              orderItem.url = "/pages/container/order-details/order-details?order_id=" + orderItem.order_id + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 1) {
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 2) {
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            } else if (orderItem.order_label == 3) {
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
            orderItem.order_state_text = '已收货';
          }else if (orderItem.order_state == 50) {
            if (orderItem.order_label == 2) {
              orderItem.order_state_text = "待发货";
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
          } else if(orderItem.order_state == 60 ){
            if (orderItem.order_label == 3) {
              orderItem.order_state_text = "众筹成功";
              orderItem.url = "/pages/container/order-details/order-details?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
          } else if (orderItem.order_state == 70){
            if (orderItem.order_label == 5 || orderItem.order_label == 6 || orderItem.order_label == 7) {
              orderItem.order_state_text = '待服务';
              orderItem.url = "/pages/container/order-details/activity-payed/activity-payed?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
          } else if (orderItem.order_state == 80) {
            if (orderItem.order_label == 5 || orderItem.order_label == 6 || orderItem.order_label == 7) {
              orderItem.order_state_text = '已完成';
              orderItem.url = "/pages/container/order-details/activity-payed/activity-payed?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
          } else if (orderItem.order_state == 90) {
            if (orderItem.order_label == 5 || orderItem.order_label == 6 || orderItem.order_label == 7) {
              orderItem.order_state_text = '已付定金';
              orderItem.url = "/pages/container/order-details/activity-payed/activity-payed?all_no=" + orderItem.all_no + "&order_label=" + orderItem.order_label;
            }
          }
          // 整理字段统一 商品活动与商品字段不统一
          if (orderItem.order_goods_info.item_image) {
            orderItem.order_goods_info.goods_image = orderItem.order_goods_info.item_image;
            orderItem.order_goods_info.goods_name = orderItem.order_goods_info.item_name;
            orderItem.order_goods_info.goods_num = orderItem.order_goods_info.item_num;
          }
          if (orderItem.order_label == 5 || orderItem.order_label == 6 || orderItem.order_label == 7) {
            orderItem.order_goods_info.goods_spec = tool.formatDate(new Date(orderItem.activity.ua_start_time * 1000), "yyyy-mm-dd");
          }
          if (orderItem.order_label == 4) {
            orderItem.order_goods_info.goods_spec = tool.formatDate(new Date(orderItem.customized_time * 1000), "yyyy-mm-dd");
          }
        }
      }
      if (orderList && orderList.length > 0) {
        orderList = orderList.sort(function(a, b) {
          return b[0].add_time - a[0].add_time;
        });
      }
      _this.setData({
        orderList: orderList
      })
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const _this = this;
    app.getLoadData().then(function (res) {
      _this.loadPage();
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.loadPage();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})