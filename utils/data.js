const goodsData = [
  {
    "type": "recommend",
    "home": [
      {
        "title": "野生驯化大黄鱼",
        "image": "http://img.yamichefs.cn/shop/store/goods/1/1_05435999447008759.jpg",
        "price": "300",
        "type": "厨师单品",
        "sort": "general",
        "id": 100408,
        "sub_type": "pintuan"
      }, {
        "title": "茅台冰醉小龙虾",
        "url":"https://ugcws.video.gtimg.com/o0355k7dymj.mp4?sdtfrom=v1103&guid=945238b4fe2fe41eb96ba5eaddc850f4&vkey=F4CFB7C28A0F470F6D97B2FEB7D393CD81C37859F51E406574A75187E94BB68C3CFC75C0D9674BD8D7986136965474CE16BC5630D5B9D00FA841AAB09535956B3DE8B51C8884854A71F2A4534500ECB37B541CE5EB4036156D80CF322A84283AECACA20871B0A8E5EDD2A22598DA1982066E6C7B2939DFAC&platform=2",
        "price": "300",
        "type": "厨师单品",
        "id": 100882,
        "sort": "general",
        "sub_type": "shipin"
      }, {
        "title": "礼包组合yami古典三黄鸡",
        "image": "http://img.yamichefs.cn/shop/store/goods/1/1_05590758377525122.jpg",
        "price": "328",
        "type": "厨师单品",
        "sort": "general",
        "id": 100622,
        "sub_type": "zhongchou"
      }, {
        "title": "礼包组合yami古典三黄鸡",
        "image": "http://img.yamichefs.cn/shop/store/goods/1/1_05590758377525122.jpg",
        "price": "428",
        "type": "厨师单品",
        "sort": "general",
        "id": 100863,
        "sub_type": "story"
      }, {
        "title": "世纪酒王",
        "image": "http://img.yamichefs.cn/mobile/special/s0/s0_05682220409050545.jpg",
        "price": "428",
        "type": "定制佳酿",
        "sort": "general",
        "id": 100863,
        "sub_type": "general_topic"
      }
    ]
  }, {
    "type": "orderly_class",
    "home": [
      {
        "title": "18年前的冬天有人为您酿了四季 1瓶500ml",
        "sub_title": "【下单后两个工作日内发货】2000年冬酿花雕原浆全国包邮",
        "image": "http://img.yamichefs.cn/shop/store/goods/1/1_05737554827689063.jpg",
        "price": "199.00",
        "id": "100728"
      }, {
        "title": "美食家Johnny世界之选莫斯卡托 750ml/瓶,原产地酿造及装瓶DOCG等级",
        "sub_title": "【下单后两个工作日内发货】2000年冬酿花雕原浆全国包邮",
        "image": "http://img.yamichefs.cn/shop/store/goods/1/1_05664890927913213.jpg",
        "price": "168.00",
        "id": "100733"
      }, {
        "title": "宝格丽－托斯卡纳葡萄酒 1瓶",
        "sub_title": "（下单后三个工作日内发货）特别福利箱装更优惠",
        "image": "http://img.yamichefs.cn/shop/store/goods/1/1_05793505361222337.jpg",
        "price": "375.00",
        "id": "100482"
      }
    ]
  }, {
    "type": "chef",
    "home": [
      {
        "title": "碳匠餐厅行政总厨",
        "sub_title": "北京－王建光",
        "image": "http://img.yamichefs.cn/mobile/special/s0/s0_05400460568751687.jpg"
      }, {
        "title": "福朋喜来登酒店行政总厨",
        "sub_title": "北京－张伟",
        "image": "http://img.yamichefs.cn/mobile/special/s0/s0_05400461228406374.jpg"
      }, {
        "title": "东来顺行政总厨",
        "sub_title": "北京－彭建军",
        "image": "http://img.yamichefs.cn/mobile/special/s0/s0_05400461493327264.jpg"
      }, {
        "title": "嘉里大酒店行政副总厨",
        "sub_title": "北京－胡含",
        "image": "http://img.yamichefs.cn/mobile/special/s0/s0_05410748411350414.jpg"
      }
    ]
  }
]
module.exports = {
  goodsData: goodsData
}