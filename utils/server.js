const config = require("../config.js");
function __args() {
  var param = {};
  if( arguments.length === 1 && typeof arguments[0] != "string") {
    param = arguments[0];
  } else {
    param.url = arguments[0];
    if( typeof arguments[1] === "object" ) {
      param.data = arguments[1];
      param.success = arguments[2];
    } else {
      param.success = arguments[1];
    }
  }
  if (param.url.indexOf("http://") !== 0) {
    // console.log(param.url.split('/'))
    var urlArr = param.url.split('/')
    param.url = '/' +urlArr[2] + '/' + urlArr[3]
    param.url = config.baseApiUrl + param.url;
  }
  return param;
}
function __json(method, param) {
  param.method = method;
  if (method == "POST") {
    param.header = {
      'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
    };
  }
  wx.request(param);
}
module.exports = {
  getJSON: function () {
    __json('GET', __args.apply(this, arguments));
  },
  postJSON: function () {
    __json('POST', __args.apply(this, arguments));
  }
}