
const getSetData = (name, event) => {
  return event.currentTarget.dataset[name]
}
/**
 * 购物车触摸移动
 */
const _cartTouchmove = (e,that) => {
  let iconCartPosX = e.touches[0].clientX - 25;
  let iconCartPosY = e.touches[0].clientY;
  const dev = wx.getSystemInfoSync();
  const windowW = dev.windowWidth - 50;
  const windowH = dev.windowHeight;
  const screenH = dev.screenHeight
  if (iconCartPosX >= 0 && iconCartPosX <= windowW) {
    that.setData({
      iconCartPosX: e.touches[0].clientX - 25
    });
  }
  if (iconCartPosY >= 25 && iconCartPosY <= windowH - 50) {
    that.setData({
      iconCartPosY: e.touches[0].clientY - 25
    });
  }
}
function compareVersion(v1, v2) {
  v1 = v1.split('.')
  v2 = v2.split('.')
  const len = Math.max(v1.length, v2.length)

  while (v1.length < len) {
    v1.push('0')
  }
  while (v2.length < len) {
    v2.push('0')
  }

  for (let i = 0; i < len; i++) {
    const num1 = parseInt(v1[i])
    const num2 = parseInt(v2[i])

    if (num1 > num2) {
      return 1
    } else if (num1 < num2) {
      return -1
    }
  }

  return 0
}
module.exports = {
  cartTouchmove: _cartTouchmove,
  getSetData: getSetData,
  compareVersion: compareVersion,
}
