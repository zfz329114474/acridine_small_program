const server = require("/server.js");
const config = require("../config.js");
// 获取自定义属性
const getDataset = (event) => {
  return event.currentTarget.dataset
}
// 获取自定义组件传过来的数据
const _getComponentDetail = (evt) => {
  return evt.detail;
}

// 警告弹窗
const _warning = (str) => {
  wx.showToast({
    title: str,
    icon: "none"
  })
}
// 锁定点击 默认两次点击 间隔 1000ms
const _lockClick = () => {
  var nowClick = new Date();
  var lastClick = wx.getStorageSync('lastClick');
  if (!lastClick) {
    wx.setStorageSync('lastClick', nowClick.getTime());
    return true;
  } else {
    if (Math.round((nowClick.getTime() - lastClick)) > config.lock_click_time) {
      // lastClick = nowClick;
      wx.setStorageSync('lastClick', nowClick.getTime());
      return true;
    } else {
      // lastClick = nowClick;
      // wx.setStorageSync('lastClick', nowClick.getTime());
      return false
    }
  }

}
// 去除两端空格
const _trim = (str) => {
  return str.replace(/^(\s|\xA0)+|(\s|\xA0)+$/g, '');
}
// 从服务器获取用户信息
const _getMemberInfo = function(that) {
  const param = {
    member_id: wx.getStorageSync('userInfo').member_id
  };
  server.postJSON("/index/user/getmemberinfo", param, function(res) {
    if (res.data.error != 0) {
      tool.warning(res.data.message);
      return;
    }
    that.setData({
      userInfo: res.data.data
    });
    wx.setStorageSync('userInfo', res.data.data)
  });
}
// 微信获取openid 并返回用户信息
const _getUserInfo = (cb) => {
  // 登录
  wx.login({
    success: res => {
      var openId = wx.getStorageSync('openId');
      var _this = this;
      // if (!openId) {
      server.getJSON("/index/index/openid", {
        code: res.code
      }, function(res) {
        if (res.data.error !== 0) {
          wx.showToast({
            title: res.message,
            icon: 'none'
          });
          return;
        }

        wx.setStorage({
          key: 'openId',
          data: res.data.data.openid,
        });
        cb(res.data.data);
      });
    }
    // }
  });
}
// 成功弹窗
const _success = (str) => {
  wx.showToast({
    title: str
  })
}
// 校验手机号
const _isValidMobile = (str) => {
  if (str.length == 0) {
    return false;
  }
  var re = /^1\d{10}$/;
  if (re.test(str)) {
    return true;
  } else {
    return false;
  }
}
// 根据时间戳获取获取标准时间 如2000-10-1 12:11:00
function _formatDate(date, fmt) {
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  let o = {
    'm+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'i+': date.getMinutes(),
    's+': date.getSeconds()
  }

  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + ''
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : _padLeftZero(str))
    }
  }
  return fmt
}

// 左边0补位 
function _padLeftZero(str) {
  return ('00' + str).substr(str.length)
}
// input输入变化，取值
function _inputChange(evt, that) {

  return {
    name: evt.target.dataset.name,
    value: _trim(evt.detail.value)
  }
}

function _changInput(evt, that) {
  that.setData({
    [evt.target.dataset.name]: _trim(evt.detail.value)
  });
}
module.exports = {
  changInput: _changInput,
  inputChange: _inputChange,
  getComponentDetail: _getComponentDetail,
  getDataset: getDataset,
  warning: _warning,
  isValidMobile: _isValidMobile,
  success: _success,
  trim: _trim,
  padLeftZero: _padLeftZero,
  getUserInfo: _getUserInfo,
  getMemberInfo: _getMemberInfo,
  formatDate: _formatDate,
  lockClick: _lockClick
}